import React, { Component } from 'react';
import PlacesAutocomplete, {
    geocodeByAddress,
    getLatLng
} from 'react-places-autocomplete';
import './Search.css';

class Search extends Component {
    constructor(props) {
        super(props);
        this.state = {
            address: ''
        }
    }

    handleChange = (address) => {
        this.setState({ address });
    }

    handleSelect = (address) => {
        geocodeByAddress(address)
            .then(results => getLatLng(results[0]))
            .then(latLng => {
                let coords = {};

                coords.lat = latLng.lat;
                coords.lng = latLng.lng;

                // call parent function and provide coords
                if (this.props.onCoordsReceived) {
                    this.props.onCoordsReceived(coords);
                }

                console.log('Success', latLng)
            })
            .catch(error => console.error('Error', error));
    }

    render() {
        const searchOptions = {
            types: ['(regions)'],
            componentRestrictions: { country: ['au'] }
        };

        return (
            <PlacesAutocomplete
                value={this.state.address}
                searchOptions={searchOptions}
                onChange={this.handleChange}
                onSelect={this.handleSelect}
            >
                {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
                    <div>
                        <input
                            {...getInputProps({
                                placeholder: 'Search Places ...',
                                className: 'location-search-input',
                                autoFocus: true
                            })}
                        />
                        <div className="autocomplete-dropdown-container">
                            {loading && <div>Loading...</div>}
                            {suggestions.map(suggestion => {
                                const className = suggestion.active ? 'suggestion-item--active' : 'suggestion-item';
                                return (
                                    <div {...getSuggestionItemProps(suggestion, { className })}>
                                        <span>{suggestion.description}</span>
                                    </div>
                                );
                            })}
                        </div>
                    </div>
                )}
            </PlacesAutocomplete>
        );
    }
}

export default Search;
