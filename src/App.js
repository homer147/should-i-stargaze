import React, { Component } from 'react';
import SunCalc from 'suncalc';
import Search from './Search/Search';
import Weather from './Components/Weather';
import Moon from './Components/Moon';
import './App.css';

const maxDayForecast = 5,
    todaysDate = new Date();

class App extends Component {
    constructor() {
        super();
        this.state = {
            fiveDayForecast: {}
        };

        let date = todaysDate;

        for (let i = 0; i < maxDayForecast; i++) {
            let day = todaysDate.getDate(),
                newDate = date.setDate (day + i);

            date = new Date(newDate);
            this.state.fiveDayForecast[this.getDateLabel(date)] = {};
        }
    }

    handleCoordsReceived = (coords) => {
        this.calculateMoonData(coords);
    }

    render() {
        return (
            <div className = "App" >
                <Search onCoordsReceived={ this.handleCoordsReceived } />
                <Weather />
                <Moon />
            </div>
        );
    }

    getDateLabel(date) {
        const month = String("00" + date.getMonth()).slice(-2),
            day = String("00" + date.getDate()).slice(-2);

        return month.toString() + '/' + day.toString();
    }

    calculateMoonData(coords) {
        const tempForecast = { ...this.state.fiveDayForecast },
            latitude = coords.lat.toFixed(4),
            longitude = coords.lng.toFixed(4);

        let date = todaysDate,
            newDate,
            moon,
            moonTimes,
            moonRise,
            moonSet,
            moonPhase,
            phase,
            dateLabel;

        for (let i = 0; i < maxDayForecast; i++) {
            newDate = date.setDate(date.getDate() + i);
            date = new Date(newDate);
            moonTimes = SunCalc.getMoonTimes(date, latitude, longitude);
            moonRise = (moonTimes.rise.getHours() < 10 ? '0' : '') + moonTimes.rise.getHours() + ':' + (moonTimes.rise.getMinutes() < 10 ? '0' : '') + moonTimes.rise.getMinutes();
            moonSet = (moonTimes.set.getHours() < 10 ? '0' : '') + moonTimes.set.getHours() + ':' + (moonTimes.set.getMinutes() < 10 ? '0' : '') + moonTimes.set.getMinutes();
            moonPhase = SunCalc.getMoonIllumination(date);
            phase = this.getMoonPhase(moonPhase.phase);
            dateLabel = this.getDateLabel(date);
            moon = {
                rise: moonRise,
                set: moonSet,
                phase: phase
            };

            if (tempForecast[dateLabel]) {
                tempForecast[dateLabel].moon = moon;
            }
        }

        this.setState({
            fiveDayForecast: tempForecast
        });
    }

    getMoonPhase(phase) {
        var decimal = phase.toFixed(2);

        if (decimal === 0) {
            return 'New Moon';
        }

        if (decimal > 0 && decimal < 0.25) {
            return 'Waxing Crescent';
        }

        if (decimal === 0.25) {
            return 'First Quarter';
        }

        if (decimal > 0.25 && decimal < 0.5) {
            return 'Waxing Gibbous';
        }

        if (decimal === 0.5) {
            return 'Full Moon';
        }

        if (decimal > 0.5 && decimal < 0.75) {
            return 'Waning Gibbous';
        }

        if (decimal === 0.75) {
            return 'Last Quarter';
        }

        if (decimal > 0.75 && decimal < 1) {
            return 'Waning Crescent';
        }
    }
}

export default App;